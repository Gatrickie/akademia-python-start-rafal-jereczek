#!/usr/bin/python3

def my_name() -> None:
    print("Your name is Rafał")


def sum_of_3() -> None:
    print(5 + 2 + 19)


def hello_world() -> str:
    return "Cześć świecie"


def substraction(a: int, b: int) -> int:
    return a - b


class Monkey:
    pass


def new() -> Monkey:
    return Monkey()


my_name()
print("-" * 20)

sum_of_3()
print("-" * 20)

print(hello_world())
print("-" * 20)

print(substraction(20, 8))
print("-" * 20)

nemo = new()
print(isinstance(nemo, Monkey))