#!/usr/bin/python3

class Horse:
    def __init__(self, horse_breed, horse_name):
        self.horse_breed = horse_breed
        self.horse_name = horse_name
        print("This horse is {} and its name is {}."
              .format(self.horse_breed, self.horse_name))

    def __del__(self):
        self.horse_breed = ""
        self.horse_name = ""


class Parfum:
    def __init__(self, parfum_name, sex, capacity):
        self.parfum_name = parfum_name
        self.sex = sex
        self.capacity = capacity
        print("Parfume {} is for {} and its capacity is {}ml."
              .format(self.parfum_name, self.sex, self.capacity))

    def __del__(self):
        self.sex = ""
        self.parfum_name = ""
        self.capacity = 0


class Cake:
    def __init__(self, cake_name, price):
        self.cake_name = cake_name
        self.price = price
        print("{} cost {} zł.".format(self.cake_name, self.price))

    def __del__(self):
        self.cake_name = ""
        self.price = ""


plotka = Horse("Arabian", "Płotka")

black = Parfum("Black", "male", 100)

cheesecake = Cake("cheesecake", 5)