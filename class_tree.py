#!/usr/bin/python3

class Vehicle:
    pass


class Motor(Vehicle):
    pass


class Car(Motor):
    pass


class Audi(Car):
    pass


a = Vehicle()
b = Motor()
c = Car()
d = Audi()


print(isinstance(a, Vehicle))
print(isinstance(b, Vehicle))
print(isinstance(c, Vehicle))
print(isinstance(d, Vehicle))
print("-" * 20)


print(isinstance(a, Motor))
print(isinstance(b, Motor))
print(isinstance(c, Motor))
print(isinstance(d, Motor))
print("-" * 20)


print(isinstance(a, Car))
print(isinstance(b, Car))
print(isinstance(c, Car))
print(isinstance(d, Car))
print("-" * 20)


print(isinstance(a, Audi))
print(isinstance(b, Audi))
print(isinstance(c, Audi))
print(isinstance(d, Audi))
print("-" * 20)